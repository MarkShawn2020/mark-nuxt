export default {
  search: {
    placeholder: '搜索文档（按"/"聚焦）',
  },
  sidebar: {
    title: '文集',
  },
  toc: {
    title: '目录',
  },
  article: {
    github: '在GayHub上编辑：）',
  },
}
