export default {
  search: {
    placeholder: 'Search the docs (Press "/" to focus)',
  },
  sidebar: {
    title: 'Album',
  },
  toc: {
    title: 'On this page',
  },
  article: {
    github: 'Edit this page on GitHub',
  },
}
