const styleReplace = (s) => {
  const s2 = s
    .trim()
    .replace(/\s*(\S*): (\S*);\s*/g, "'$1': '$2',")
    .replace(/style="(.*?)"/m, ':style = "{$1}"')
  console.log(s2)
  return s2
}

module.exports = styleReplace

if (!module.parent) {
  var s = `style="
              display: flex;
              justify-content: space-between;
              align-items: center;
              flex-wrap: nowrap;
              width: 100%;
            "`

  s = `            style="
              display: flex;
              justify-content: space-between;
              align-items: center;
              flex-wrap: nowrap;
              width: 100%;
            "`

  styleReplace(s)
}
