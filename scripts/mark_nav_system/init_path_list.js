const fs = require('fs')
const path = require('path')

const pathRoot = path.join(__dirname, '../../content')

const DEFAULT_LANG = 'en'
const LANG_LIST = ['zh', 'en']

function genPathTree(relPath, pathName) {
  console.log('scanning relative path: ', pathName)

  var dict = {
    relPath,
    langs: {
      en: pathName,
      zh: '',
    },
  }
  var children = fs
    .readdirSync(path.join(pathRoot, relPath), { withFileTypes: true })
    .filter((item) => item.isDirectory())

  if (children.length > 0) {
    dict.children = children.map((item) =>
      genPathTree(path.join(relPath, item.name), item.name)
    )
  }
  return dict
}

function genPathList(pathRoot, relPath, pathName, dict = {}) {
  console.log('scanning relative path: ', pathName)

  dict[relPath] = {
    en: pathName,
    zh: '',
  }
  fs.readdirSync(path.join(pathRoot, relPath), { withFileTypes: true })
    .filter((item) => item.isDirectory())
    .map((item) =>
      genPathList(
        pathRoot,
        path.posix.join(relPath, item.name),
        item.name,
        dict
      )
    )
  return dict
}


// console.log(JSON.stringify(genPathTree('zh', '/'), null, 2))
const pathList = genPathList(path.join(pathRoot, 'zh'), '/', '/')
fs.writeFileSync('pathMap.json', JSON.stringify(pathList, null, 2))
