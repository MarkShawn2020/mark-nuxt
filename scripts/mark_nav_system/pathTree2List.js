const fs = require('fs')

const lang = 'zh'

const pathTreePath = `../../store/pathTree/${lang}.json`

const pathTree = require(pathTreePath)

const pathMap = {}

function genPathMap(pathTree) {
  pathMap[pathTree.path] = {
    title: pathTree.title,
    description: pathTree.description,
    path: pathTree.path,
  }
  if (pathTree.children) {
    Object.keys(pathTree.children).forEach((key) =>
      genPathMap(pathTree.children[key])
    )
  }

  return pathMap
}

genPathMap(pathTree)

console.log(pathMap)

fs.writeFileSync(
  `../../store/pathMap/${lang}.json`,
  JSON.stringify(pathMap, null, 2)
)
