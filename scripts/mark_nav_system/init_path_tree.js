const fs = require('fs')
const path = require('path')

const pathRoot = path.join(__dirname, '../../content/zh')

function genPathTree(relPath, dirName) {
  var dict = {
    langs: {
      en: dirName,
      zh: '',
    },
  }
  const children = fs
    .readdirSync(path.join(pathRoot, relPath), { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
  if (children.length > 0) {
    dict.children = {}
    children.forEach((dirent) => {
      dict.children[dirent.name] = genPathTree(
        path.posix.join(relPath, dirent.name),
        dirent.name
      )
    })
  }
  return dict
}

function genPathTree2(relPath, dirName) {
  var dict = {
    title: dirName,
    description: '',
    path: relPath,
  }
  const children = fs
    .readdirSync(path.join(pathRoot, relPath), { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
  if (children.length > 0) {
    dict.children = {}
    children.forEach((dirent) => {
      dict.children[dirent.name] = genPathTree2(
        path.posix.join(relPath, dirent.name),
        dirent.name
      )
    })
  }
  return dict
}

const t = genPathTree2('/', 'Navigate From Here')
const tj = JSON.stringify(t, null, 2)
console.log(tj)
fs.writeFileSync('path-tree-en.json', tj)
