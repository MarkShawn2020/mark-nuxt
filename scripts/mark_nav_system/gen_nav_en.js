const navZh = require('./nav.json')
const navEn = require('./nav-en.json')
const deepEqual = require('deep-equal')

function getDataNew(id, navFrom, navTo) {
  if (!id in navTo) return navFrom[id]
  const dataZh = navFrom[id]
  const dataEn = navTo[id]
  var dataNew = {}
  Object.keys(dataZh).forEach((key) => {
    // console.log({ key })
    if (deepEqual(dataZh[key], dataEn[key])) return (dataNew[key] = dataZh[key])
    console.log(
      `detected key ${key} change from ${dataZh[key]} to ${dataEn[key]}`
    )
    return (dataNew[key] = dataEn[key])
  })
  // console.log({ dataNew })
  return dataNew
}

function getNavNew(navFrom, navTo) {
  var navNew = {}
  Object.keys(navFrom).forEach((id) => {
    // console.log({ id, dataZh, dataEn })
    navNew[id] = getDataNew(id, navFrom, navTo)
  })
  console.log(navNew)
  return navNew
}

getNavNew(navZh, navEn)
