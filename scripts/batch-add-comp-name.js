const fs = require('fs')

const updateVue = (dirPath) =>
  fs.readdirSync(dirPath).forEach((compName) => {
    fs.readFile(compName, 'utf-8', (err, data) => {
      if (err) throw err
      if (/<script/.test(data)) return
      data += `

<script>
  export default {
    name: '${compName.replace('.vue', '')}'
  }
</script>
    `
      // console.log({ data })
      fs.writeFileSync(compName, data)
      console.log('successfully write data into: ', compName)
    })
  })

if (!module.parent) {
  updateVue(
    'E:\\MyProjects\\JSProjects\\VueProjects\\mark-nuxt\\content-theme-docs\\src\\components\\global\\icons'
  )
}
