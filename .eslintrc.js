module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:vue/essential',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended',
  ],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    // 为便于开发，抑制未使用组件的报错
    'vue/no-unused-components': 'off',
    'vue/require-default-prop': 'off',
    'vue/no-v-html': 'off',
    'no-unused-vars': 'off',
    'no-console': 'off',
    'no-eval': 'off',
  },
}
