/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
const path = require('path')
const plugin = require('tailwindcss/plugin')
const defaultTheme = require('tailwindcss/defaultTheme')
const selectorParser = require('postcss-selector-parser')

const defaultVariants = require('./assets/css/tailwindcss/default_variants.js')
const customVariants = require('./assets/css/tailwindcss/custom_variants.js')
const customTypography = require('./assets/css/tailwindcss/custom_typography')

module.exports = {
  theme: {
    extend: {
      fontFamily: {
        sans: ['DM Sans', ...defaultTheme.fontFamily.sans],
        mono: ['DM Mono', ...defaultTheme.fontFamily.mono],
      },
      colors: {
        primary: {
          100: '#E6FAF2',
          200: '#BFF3E0',
          300: '#99EBCD',
          400: '#4DDCA7',
          500: '#00CD81',
          600: '#00B974',
          700: '#007B4D',
          800: '#005C3A',
          900: '#003E27',
        },
      },
      maxHeight: {
        '(screen-16)': 'calc(100vh - 4rem)',
      },
      inset: {
        16: '4rem',
      },
    },
    typography: customTypography,
  },
  variants: Object.assign(defaultVariants, customVariants),
  plugins: [
    plugin(function ({ addVariant, prefix, e }) {
      addVariant('dark', ({ modifySelectors, separator }) => {
        modifySelectors(({ selector }) => {
          return selectorParser((selectors) => {
            selectors.walkClasses((sel) => {
              sel.value = `dark${separator}${sel.value}`
              sel.parent.insertBefore(
                sel,
                selectorParser().astSync(prefix('.dark-mode '))
              )
            })
          }).processSync(selector)
        })
      })

      addVariant('dark-hover', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.dark-mode .${e(`dark-hover${separator}${className}`)}:hover`
        })
      })

      addVariant('dark-focus', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.dark-mode .${e(`dark-focus${separator}${className}`)}:focus`
        })
      })
    }),

    // https://tailwindcss.com/docs/extracting-components#extracting-css-components-with-apply
    // 配置图标，贼有用！
    plugin(function ({ addComponents }) {
      const Icons = {
        '.Icon': {
          width: '1.5rem',
          height: '1.5rem',
          fontSize: '1.5rem',
          margin: '0.5rem',
          cursor: 'pointer',
        },
        '.Icon-text': {
          color: '#999',
          '&:hover': {
            color: '#00CD81',
          },
        },
        '.dark-mode .Icon-text': {
          color: '#eee',
          '&:hover': {
            color: '#00CD81',
          },
        },
      }

      const Containers = {
        '.Container': {
          padding: '0.5rem',
          // margin: '0.5rem',
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center',
          // alignItems: 'center',
          '@media (min-width: 768px)': {
            padding: '0.75rem',
            // margin: '0.75rem',
          },
          '@media (min-width: 1024px)': {
            padding: '1rem',
            // margin: '1rem',
          },
        },
      }
      addComponents(Icons)
      addComponents(Containers)
    }),

    require('@tailwindcss/typography'),
  ],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'content/**/*.md',
      path.join(__dirname, 'components/**/*.vue'),
      path.join(__dirname, 'layouts/**/*.vue'),
      path.join(__dirname, 'pages/**/*.vue'),
      path.join(__dirname, 'plugins/**/*.js'),
      'nuxt.config.js',
    ],
    options: {
      whitelist: ['dark-mode'],
    },
  },
}
