module.exports = {
  backgroundColor: ['responsive', 'hover', 'focus', 'dark', 'dark-hover'],
  textColor: ['responsive', 'hover', 'focus', 'dark', 'dark-hover'],
  borderColor: ['responsive', 'hover', 'focus', 'dark', 'dark-hover'],
  borderStyle: ['responsive', 'hover', 'focus', 'dark', 'dark-hover'],
  margin: ['responsive', 'last'],
  borderWidth: ['responsive', 'first', 'last'],
  typography: ['responsive', 'dark'],
}
