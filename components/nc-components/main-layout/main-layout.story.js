import MainWithAsides from './main-layout.vue'

export default { title: 'main-layout' }

export const component = () => ({
  components: { MainWithAsides },
  props: {},
  template: '<main-layout />',
})
