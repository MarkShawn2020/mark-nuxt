const deepEqual = function (x, y) {
  // [CSDN博客_判断两个对象是否相等](https://blog.csdn.net/weixin_34315485/article/details/91435898 )
  // 指向同一内存时
  if (x === y) {
    return true
  } else if (
    typeof x === 'object' &&
    x != null &&
    typeof y === 'object' &&
    y !== null
  ) {
    if (Object.keys(x).length !== Object.keys(y).length) return false

    let prop
    for (prop in x) {
      if (prop in y) {
        if (!deepEqual(x.prop, y.prop)) return false
      } else return false
    }

    return true
  } else return false
}

export default deepEqual
