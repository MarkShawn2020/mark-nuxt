async function searchArticle(key) {
  const item = {}
  item.detail = await $api
    .get('/wx/articles', { params: { key: '谨以此文' } })
    .then((res) => res.data[0])
  item.content = await $api
    .get('/wx/article/' + item.detail._id)
    .then((res) => res.data.content[0])
  console.log({ item })
  return item
}

export default searchArticle
