const safeAssign = function (state, payload) {
  if (payload) {
    // console.log("assign payload", payload)
    Object.keys(payload).forEach((key) => {
      if (payload[key] !== undefined) {
        state.user[key] = payload[key]
      }
    })
  }
}

export default safeAssign
