function deepCopy(j) {
  return JSON.parse(JSON.stringify(j))
}

export default deepCopy
