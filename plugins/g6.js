import Vue from 'vue'
import G6 from '@antv/g6'

export default function ({ app }, inject) {
  inject('G6', G6)
}
