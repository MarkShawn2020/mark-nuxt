const CONTROL = {
  ui: {
    menu: {
      showDescription: false,
    },
  },
}

export default function ({ app }, inject) {
  inject('control', CONTROL)
}
