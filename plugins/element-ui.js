import Vue from 'vue'

// import Element from 'element-ui'
// Vue.use(Element, { locale })

/**
 * 按需导入参考：[Nuxt配置Element-UI按需引入的操作方法_javascript技巧_脚本之家](https://www.jb51.net/article/190150.htm)
 */
import {
  // 通过use使用的组件
  Button,
  Drawer,
  Icon,
  Avatar,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Card,
  Input,
  Dialog,
  Popover,
  Popconfirm,
  Form,
  FormItem,
  Divider,
  Upload,
  Container,
  Collapse,
  CollapseItem,
  Alert,
  Image,
  Loading,

  // 需要全局注册的组件
  Notification,
  Message,
} from 'element-ui'

const elementsUse = [
  Button,
  Drawer,
  Icon,
  Avatar,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Card,
  Input,
  Dialog,
  Popover,
  Popconfirm,
  Form,
  FormItem,
  Divider,
  Upload,
  Container,
  Collapse,
  CollapseItem,
  Alert,
  Image,
  Loading,
]

elementsUse.forEach((ele) => Vue.use(ele))

Vue.prototype.$notify = Notification
Vue.prototype.$message = Message
