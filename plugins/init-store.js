export default async function ({ store, app }) {
  if (process.server) {
    await store.dispatch('fetchSettings')
    await store.dispatch('fetchPathTree')
  }
  // Hot reload on dev
  if (process.client && process.dev) {
    window.onNuxtReady(() => {
      window.$nuxt.$on('content:update', async () => {
        await store.dispatch('fetchSettings')
        await store.dispatch('fetchPathTree')
      })
    })
  }
}
