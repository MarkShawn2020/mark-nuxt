const md5 = require('js-md5')

const getMd5 = (val) => {
  const hash = md5.create()
  hash.update(val)
  return hash.hex()
}

export { getMd5 as md5 }

export default function ({ app }, inject) {
  inject('md5', getMd5)
}
