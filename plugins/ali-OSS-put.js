const OSS = require('ali-oss')
const aliOss = require('../.env.json')['ali-oss-RAM']

const AK = aliOss.AK
const SK = aliOss.SK
const REGION = 'oss-cn-hangzhou'
const BUCKET = 'mark-vue-oss'
const PREFIX = 'blogs/'

const client = new OSS({
  region: REGION,
  accessKeyId: AK,
  accessKeySecret: SK,
  bucket: BUCKET,
})

/**
 * object-name可以自定义为文件名（例如file.txt）或目录（例如abc/test/file.txt）的形式，实现将文件上传至当前Bucket或Bucket下的指定目录。
 * @param fileKey
 * @param fileData
 * @returns {Promise<*>}
 */
async function putImg(fileKey, fileData) {
  fileKey = PREFIX + fileKey.toString()
  console.log('fileKey', fileKey)
  try {
    const result = await client.put(fileKey, fileData)
    console.log(result.url)
    return result.url
  } catch (e) {
    console.log(e)
  }
}

export default function ({ app }, inject) {
  inject('putImg', putImg)
}

/*
sample:
http://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/static/logos/mark-logo-small.png
 */
