const api = {
  root: 'https://nanchuan.site/api/v1',
  apps: {
    user: {
      register: {
        path: '/user/register',
      },
      login: {
        path: '/user/login',
      },
      logout: {
        path: '/user/logout',
      },
      token: {
        path: '/user/token',
      },
      home: {
        path: '/user/home',
      },
      update: {
        path: '/user/update_detail',
      },
      delete: {
        path: '/user/delete',
      },
    },
    github: {
      rankingAll: {
        path: '/github/github/all',
      },
      rankingCn: {
        path: '/github/github/cn',
      },
    },
    oss: {
      upload: {
        path: '/oss/upload',
      },
    },
    post: {
      comment: {
        add: {
          path: '/post/comment/add',
        },
        query: {
          path: '/post/comment/query',
        },
        delete: {
          path: '/post/comment/delete',
        },
      },
    },
  },
}

export default function ({ app }, inject) {
  inject('api', api)
}
