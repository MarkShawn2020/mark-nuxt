import Vue from 'vue'
import NcLoginDialog from '~/components/login-comp/NcLoginDialog'

const Login = function (options) {
  const LoginConstructor = Vue.extend(NcLoginDialog)
  options = options || {}
  const instance = new LoginConstructor({ data: options })
  instance.$mount()
  document.body.appendChild(instance.$el)
  instance.visible = true
  return instance
}

export default function ({ app }, inject) {
  inject('login', Login)
}

// Vue.prototype.$login = Login
