async function getComments(options) {
  return await this.$http.$get(this.$api.apps.post.comment.query.path, {
    params: options,
  })
}

export default function ({ app }, inject) {
  inject('getComments', getComments)
}
