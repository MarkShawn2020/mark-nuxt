---
title: vuepress关于永久链接与中文路由
menuTitle: 永久链接与中文路由
description: Vuepress
relativePath: zh/文集/计算机应用研究/Vuepress/vuepress永久链接与中文路由.md
slug: /文集/我为什么放弃了vuepress
tags:
  - Vuepress
category: Vuepress
cover: ''
date: ''
position: 797
version: 0.1.0
fullScreen: false
---



## 永久链接

官网对于永久链接的设定不甚详细。

如下，我也搞不清这个文章发布是怎么算的。

![image-20200816093945554](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816093945554.png)

翻了一下源代码，在`node_modules/@vuepress/shared-utils/lib/getPermalink.js`里有如下，生成链接的函数接收五个参数，其中第三个参数就是时间，这些模板变量都是从这个时间中解析出来的。

![image-20200816094124718](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816094124718.png)



接着定位到了`node_modules/@vuepress/core/lib/node/Page.js`

![image-20200816094645568](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816094645568.png)



而这个`this.date`呢，是根据这个函数生成的。

![image-20200816094813159](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816094813159.png)



最后到了`node_modules/@vuepress/core/lib/node/util/index.js`

![image-20200816094959257](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816094959257.png)



注意这个`DATE_RE`，我拷贝出来一下：` /(\d{4}-\d{1,2}(-\d{1,2})?)-(.*)/`，所以这个程序的意思就是，依次从frontmatter.date、文件名和文件夹名中提取`YYYY-M?M-D?D-FILENAME`的格式，一旦提取到，就会生成它的永久链接。

尴尬了，我原先素材的取名都是`YYMMDD`的，例如：

![image-20200816095412067](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816095412067.png)



于是，写了个脚本，重命名了一下。

![image-20200816100625252](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816100625252.png)



接下来就是永久链接的路由配置，由于我们的nav和sidebar是自己自己写的，所以需要自己对应上。

按照规则，现在的文章路径，以`2020-05-26-杨季.md`为例，变成了`/2020/05/26/杨季`。

但专辑名也要相应地换一下，目前文件夹名是`A01-考研专辑`，对应的`index.md`，专辑名显然不能换，但我们也想用一个好记的路径，因此直接在`index.md`里设置frontmatter，这里就不要用`date`变量了，而是直接`permalink`，如下。

![image-20200816101325695](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816101325695.png)



但这样做有个弊端，我们在生成`menus`和`sidebar`时需要先解析文件内容，与我一开始简单地通过文件路径生成网络路由的初衷不符，虽然`vuepress`本身便是基于此的。

<tip> 
如上标准化文件名后，还要在`config.js`中，配置永久路径的模板变量。

![image-20200816102550549](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816102550549.png)

这样才能被`vuepress`解析。

 </tip>



## 路由的中文转义

程序内部对每个路由会有三种形式：`relativePath | regularPath | path`，其中`relativePath`是本地文件路径，`regularPath`是转义后的路径，而`path`顾名思义应该是最终的路径。

此外，还有一个`permanlink`，是用户指定的文件的最终访问路径。

在我们的程序中，直接按照文件路径配置nav和sidebar是比较符合common sense的，操作起来也是比较容易，只要用`fs`遍历或者递归即可。**这样的操作，它本身不应该有问题，如果有问题，那一定是框架的问题**。

### 未配置permalink的结果

:white_check_mark: vuepress生成了`.html`结尾的目标节点，该路径的中文形式就是本地文件路径，具体网络路径为`/zh/S01-%E6%96%87%E9%9B%86/G01-%E7%A0%94%E7%A9%B6%E7%94%9F%E5%8D%87%E5%AD%A6%E6%A1%88%E4%BE%8B/A02-%E7%95%99%E5%AD%A6%E4%B8%93%E8%BE%91/2020-06-12-%E7%8E%8B%E9%80%B8%E6%88%90.html`![image-20200816150514054](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816150514054.png)



:white_check_mark: 当直接访问上条`.html`结尾的路径时，会跳转到没有`index.html`结尾的路径，对应的就是本地文件路径。

![image-20200816150727187](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816150727187.png)



:white_check_mark: 最后，若直接访问中文路径，或者访问该转义路径，是等价的。

![image-20200816150829410](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816150829410.png)

### 配置了permalink的结果

:white_check_mark: 此时，如果直接访问`/考研专辑/`或者`/%E8%80%83%E7%A0%94%E4%B8%93%E8%BE%91/`是可以成功跳转的。

![image-20200816144956705](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816144956705.png)



:x: 然而虽然下面的路径`/%E8%80%83%E7%A0%94%E4%B8%93%E8%BE%91/index.html`，根据路由显示也是跳转上面的路径，但是事实上，当我们输入网址后，会被浏览器解析成`/考研专辑/index.html`，从而失配。

![image-20200816145024359](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816145024359.png)



:x: 紧接着，即我们程序中设定的本地文件路径映射，虽然也显示跳转，但会直接转义，导致跳转失败。

![image-20200816145042228](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816145042228.png)





值得注意的是，如果文件路径都是英文，配置了`permalink`之后，路径的跳转都没问题。

有问题的点在于，当使用转义后的本地文件路径去访问时，浏览器指向了转义前的路径（即中文），而该路径是没有跳转项的，导致跳转失败。

所以，问题的解决就在于，**能否配置一个转义前（中文）的本地文件路径，使之跳转到最终的permalink**。

或者，**不要转义本地文件路径**。

### 修改源代码

如下，对`node_modules/@vuepress/core/lib/node/internal-plugins/routes.js`转义后的路径重新解码，保证原文件路径对最终路径的映射，即可成功解决中文路径跳转问题。

![image-20200816155711710](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816155711710.png)



## 总结、担忧与考虑

其实标准化url也是很好的一种设计，能让程序更加地稳健，但却出现了我遇到的permalink强行跳转导致转义失配的结果。我解决的方法是对标准化之前的文件路径建立映射。

在我的项目中，目前还不存在由于url编码而产生歧义的可能，其他项目可能要自行把握。也希望看到更优秀的解决方案！



