---
sidebarRecursive: auto
title: Leetcode Training
menuTitle: Leetcode Training
description: Leetcode修炼
relativePath: zh/文集/编程世界/Leetcode修炼/leetcode-training.md
slug: /文集/Leetcode修炼/
tags:
  - 计算机与软件
category: CS
cover: ''
date: ''
position: 80
version: 0.1.0
fullScreen: false
---



<tip> LeetCode is the best platform to help you enhance your skills, expand your knowledge and prepare for technical interviews. *(copied from the official site)*
 </tip>

## 官网链接
- [LeetCode中文](https://leetcode-cn.com/)
- [LeetCode英文](https://leetcode.com/)

