---
title: vuepress的运行原理
menuTitle: 运行原理
description: Vuepress
relativePath: zh/文集/计算机应用研究/Vuepress/vuepress的运行原理.md
slug: /文集/vuepress的运行原理
tags:
  - Vuepress
category: Vuepress
cover: ''
date: ''
position: 795
version: 0.1.0
fullScreen: false
---



## 服务端视角

### 启动

启动时首先使用`npm run docs:dev`命令，即调用的`packages.json`文件中的`vuepress dev docs`命令。

![image-20200812195621125](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812195621125.png)



`vuepress`指向`@vuepress/core`。

![image-20200812200434428](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812200434428.png)



创建了一个`.node/App`程序（即服务端），并且启动了`createApp(options)`,`app.process`、`app.dev()`等几个命令。

由于我们直接使用`vuepress run dev`启动的，所以`options`为空。

![image-20200812203534119](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812203534119.png)



### App.js

`App.js`文件中就是一个App类，导入了很多依赖，比如生成`Markdown`的，生成`Page`组件的，以及与`dev`、`build`相关的进程函数。

仔细看之前的`CreateApp(options)`函数，其实就是`App(options)`函数，经过该函数进行初始化。

![image-20200812204059251](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812204059251.png)



`app.process()`函数中用了异步函数，主要用于解析页面之类。

![image-20200812204540636](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812204540636.png)



### DevProcess

`dev`函数中主要是使用`DevProcess`进行，并且会监听本地文件的变动，这里的本地文件是被程序特定的一些文件，主要是`markdown`、`vue`之类的。

与之对比，可以看一下生产环境的启动函数。

![image-20200812204720553](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812204720553.png)



可以看到，要简洁地多，主要是就是运行，然后渲染。

![image-20200812204855624](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812205235678.png)



再看一下这个`DevProcess`函数。

![image-20200812205235678](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812205344580.png)



使用了`events`包进行事件驱动，而且所有的`webpack`打包都是在这里进行的，详见如下：

![image-20200812205344580](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812204855624.png)



### 文件监控

接下来看它的监控范围，首先是对`.js`、`.md`和`.vue`的监控。

![image-20200812210030040](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812210030040.png)

可以看到`.md`是根目录下全局监控，而`.vue`只限于在`.vuepress/components`下定义的。



<tip> 
暂时还不清楚各类监控的设定缘由与效果差异

 </tip>



其次是对`.vuepress`文件夹下的`config`文件进行监控，支持`.js`、`.yml`和`.toml`三种格式，是写死的，所以`.yaml`、`.json`这两种格式是不被监控的，这里写的有点死了，用正则会更好一些，不过如果想监控自己的其他文件可以配置`siteConfig.extraWatchFiles`字段，参见：[配置 | VuePress（#extraWatchFiles）](https://vuepress.vuejs.org/zh/config/#extrawatchfiles)



1. 我的项目配置的就是`.yaml`格式，在了解到`.yaml`配置文件不在监控列表内后，我改成了`.yml`结尾，结果发现`js-yaml`不能完整解析`.yml`格式。

2. 基于此，我去修改了`vuepress`的源码，以使它支持`config.yaml`的监控。

3. 但最后我发现了`siteConfig.extraWatchFiles`这个配置，所以我最后采用了这个，基于此，我现在的`config.js`里就多了如下的监控文件：

   ![image-20200813031450552](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200813031450552.png)



此外，还对frontmatter进行监控，如下：

![image-20200812210245060](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812210245060.png)



### 打包与服务器

接着就是使用`webpack`进行一些资源的打包与解析，比如`html`。

![image-20200812210428181](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812210428181.png)



接着是使用`express`创建服务器。

![image-20200812210723659](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812210723659.png)

可以看到，公共资源都在`./public`下，而以`/`结尾的如果匹配不到文件，则会自动寻找`index.html`文件，我之前在配置`sidebarRecursive`的时候，就经常出现`index.html`匹配不上的报错，我后来的做法是给每个有路由的文件夹都配一个`index.md`文件占位，并且在解析`sidebarRecursive children`的时候排除这个`.md`文件（也可以不排除，就是像官方一样留空就可以，但这样会使该文件夹下的其他文件与它同层级，这不是我想要的）。



最后是一些`host`和 `port`等的配置。

![image-20200812211219741](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200812211219741.png)



至此，服务器就创建成功了，打开浏览器地址可以看到所渲染出来的默认页面。

那从客户端视角来看，这一切又是如何呈现的呢？



## 客户端视角

### 组件依赖图

首先，我们分析一下默认的主题，它位于`node_modules/@vuepress/theme-default`。

首先使用`webstorm`生成一下组件的依赖图。

![image-20200813023649261](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200813023649261.png)

可以看到，所有的一级组件都经由`util/index.js`展开，二级组件再被一级组件所单独或共享使用。不过`util/index.js`中只是定义了各个解耦的被调用函数，直接看并没有什么欧诺个，所以我们还是先从`layouts/Layout.vue`看起。



### Layout

可以看到，组件如官网所述，主要分`components`、`global components`、`layouts`三大块，其中`layouts/Layout.vue`是默认入口。

<tip> 
这个组件在哪定义为入口的，我还没找到，所以先看组件吧。

 </tip>

![image-20200813021205905](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200813021205905.png)

在这个`Layout.vue`中，主要由以下组件构成

```bash
.theme-container
	- Navbar
	- Sidebar
		- .sidebarRecursive-mask
		- v-slot:sidebarRecursive-top
		- v-slot:sidebarRecursive-bottom
	- <View>
		- Home
		- Page
			- v-slot:page-top
			- v-slot:page-bottom
```

其中最关键的就是`Home`与`Page`的分支判断，这其实就是对应两种`View`，而依据就是`frontmatter`中`home`字段是否为真。

了解到这点之后就很清楚了，如果我们想多设几种`View`，只要再这里修改`if...else...`的逻辑就可以了。

<tip> 
在VuePress官网里并没有`View`的概念，这个概念来自于其他一些框架，比如`Nuxt`、`Gatsby`之类，我引入到这里，是因为VuePress默认的主题将组件按照作用域划分分组，对实际开发来说体验不是很好。我的想法是，`Nav`、`Sidebar`、`Footer`之类都可以作为通用组件，而内容主区域可以有多个呈现，比如展示`Home`、`Page`，以及`PageList`等等其它种可能。

基于此，我尝试将`Home`和`Page`分离出来独立到`View`文件夹内，这样直接`dev`模式运行是没有问题的。但是！打包发布却出现了严重的问题！

所以，还是乖乖地放在`components`里面吧。。。

最后，关于父（`@parenttheme`）子（`@theme`）主题的引用，请参考：[主题的继承 | VuePress](https://vuepress.vuejs.org/zh/theme/inheritance.html#%E7%BB%84%E4%BB%B6%E7%9A%84%E8%A6%86%E7%9B%96)

 </tip>


