---
title: vuepress使用体验
menuTitle: 使用体验
description: Vuepress
relativePath: zh/文集/计算机应用研究/Vuepress/vuepress使用体验.md
slug: /文集/vuepress使用体验
tags:
  - Vuepress
category: Vuepress
cover: ''
date: ''
position: 796
version: 0.1.0
fullScreen: false
---



## 设计相关

### 覆写设定不合理

以页面导航为例，在组件中判断一个页面的上一页或者下一页是什么，需要通过多个层级的多次判断，比如先判断主题有没有上下页，再判断页面是否有上下页，判断的位置隐含在`links`里或者`frontmatter`中。

作为`vuepress`的使用者，我经常需要在`console`中打印出当前页面的变量，以判断什么变量出于什么作用域范围内，我想说，这不是必要的。如果作者采用更加友好的设计方式，在后端与中端过程中将组件完全覆写，可以大大减少前端的逻辑判断。

也许这会束缚前端`vue`组件开发的灵活度，但框架毕竟是框架，使用`vuepress`的开发者应有能力在程序的配置阶段把一切设定好（如果框架机制允许的话）。也许我更崇尚配置式的框架吧，例如爬虫领域的`python`框架`scrapy`。

![image-20200815012805614](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/20200815012805.png)



## 配置相关

### 多语言配置（这是真坑）



![image-20200814002121393](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/image-20200814002121393.png)





### 使用自己的主题

在没有别人主题或者自己暂时没有经验的情况下，可以先把`vuepress`默认的主题拷贝出来，路径是在`node_modules/@vuepress/theme-default`，在配置的时候有几点一定要注意（至少是对于新手来说）。
1. 将主题文件夹放在`docs/.vuepress`下，一定一定！不然`dev`可以运行，`build`一定会出错！
2. 修改`index.js`里的内容：
```js
module.exports = {
  extend: '@vuepress/theme-default'
}
```
3. 按照官网要求，重命名为`vuepress-theme-YOUR-THEME-NAME`
4. 修改`.vuepress/config.js`里的内容：
```js
module.exports = {
  theme:  require.resolve('./vuepress-theme-YOUR-THEME-NAME'),
  ...
}
```

### config.yaml 或 config.yml

经过测试，`js-yaml`对`.yml`解析的支持有限，因此推荐使用`.yaml`。

如需使用，请在`config.js`的`extraWatchedFiles`字段中加入相应的监控文件路径。

## 运行相关

### navbar的下划线

在`element-ui`中，只要成功点击了某导航项的切页，无论是单链接是子链接，下划线都会更新到那一项上。

但我发现`vuepress`里没有实现，需要解决一下。

例如当我点击了我的`博客`下拉菜单中某一项后，页面成功跳转，但下划线没了。

![image-20200813032635390](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/20200815012640.png)

与之对比的是，如果是在首页，则会有。

![image-20200813032701945](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/20200815012641.png)

### sidebar的热更新

在我更新我的本地文档时，已经启动的`vuepress`可以在两秒内监测到内容的变化，并相应地更新前端的呈现，但是对应的`siderbar`则不会自动更新。

按道理，`toc`是从文档内容中提取出的，`sidebarRecursive`基于`toc`呈现，理应做到同步更新，这是值得优化的一个点。



### frontmatter格式问题



![image-20200816121349543](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200816121349543.png)

## 插件相关

### [@vuepress/plugin-nprogress](https://vuepress.vuejs.org/zh/plugin/official/plugin-nprogress.html)

这个插件是用来显示页面跳转的时候顶部加载的进度条，如下所示。

![image-20200815193651544](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200815193651544.png)

当网站刚启动的时候，或者网速很慢的时候，这个进度条就会比较明显。

不过，由于网站以静态内容为主，正常情况下，这个条只会一闪而过。
