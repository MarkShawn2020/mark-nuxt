---
title: BUG FIX
menuTitle: BUG FIX
category: development

position: 9
fullScreen: false
---

## 【已解决】BUG: 在markdown文件中引用vue组件，props不能成功传入的问题
### 现场

这是因为在读取markdown文件过程中，变量名自动取成了小写，导致原先定义的驼峰式prop变量无法正常赋值。

具体而言就是，在markdown中`:wordList`会变成`:wordlist`，而原先的vue组件中则会保持`wordList`的prop
不变，这导致了vue组件多了一个`wordlist`属性，而`wordList`则为空。

### 解决方案

## 【部分解决】BUG: content-theme-docs中关于index.md的路由问题

### 现场

![image-20200818171926502](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200818171926502.png)

### 解决方案
使用`index.vue`等page页面去定义这些路由即可。


## 【已解决】BUG: 关于frontmatter中未定义fullScreen字段，导致全屏切换组件无效的问题

### 现场
我封装了一个点击切换全屏的组件，它依赖于父组件（即`nuxt-content`页面的`document.fullScreen`属性）。

我在测试的过程中发现了问题，原因是我的一些页面没有初始化`fullScreen`的`frontmatter
`，而我的切换逻辑仅仅是简单的取反，即`document.fullScreen=!document.fullScreen`。

我修改这个逻辑，希望它变得更加稳健，即`document.fullScreen=!(document.fullScreen || false)`。

然而，并不奏效，我忽视了我靠切换`fullScreen`属性以改变网页展示的行为，依赖于vue对data元素的监听。

由于我们的markdown文件事先并没有写入`fullScreen`，因此导致vue并没有监听到该键，所以之后的操控行为并非响应式的。

### 解决方案
第一种解决方案就是修改原来的markdown文件，使之包含一个默认的初始值，比如`fullScreen: false`。

第二种解决方案，则是修改一下生成`frontmatter`或者解析`frontmatter`步骤的代码，使`frontmatter`或者`document
`有着合理的默认值。此外，对于不规范的markdown文档，还应在console中进行warning提示。


## 【已解决】BUG: content-theme-docs中关于index键重复的问题

### BUG初见面

我在使用`nuxt-content`官方的`content-theme-docs`主题时，遇到了浏览器警告，提示我`index`键重复。

![image-20200818163726511](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200818163726511.png)



这样的报错，我是大概知道什么原因的。

这是因为我刚刚从`vuepress`框架迁移到`nuxt`框架上来，`vuepress`需要每个文件夹下有一个`index.md`文件用以渲染该文件夹路径，而当访问这些没有`index.md`的文件夹路径时，将失败。由于这些文件夹路径大多又和导航栏、侧边栏相绑定，耦合很高，极易出现意想不到的bug。

`nuxt-content`虽然也是基于文件路径，但是给出了更加灵活的路由方案，因此不需要如此大费周章。

基于此，**我直接删除或者重命名了博客文件夹下所有（除首页）的`index.md`文件**。

于是，报错不再发生了。

### BUG再见面

尽管如此，意外还是继续发生了。

同上，如果我直接访问一个没有`inedx.md`的文件夹路径，`nuxt-content`也会报出`page not found`的问题，只不过，由于`content-theme-docs`主题里的侧边栏设计的比较巧妙，即**文件夹路径默认不可点击**，因此发觉不了这样的问题。

但是，现在有一项新的需求：**没有index.md文件将无法实现面包屑导航的功能**。

即当从`AAA/xxx.md`文件导航回`AAA`文件夹时，由于该文件夹下没有`index.md`文件，将导致导航失败。

基于此认识，如果我们想让自己的文件导航系统更加稳健，我们还是应当采取`vuepress`的管理方案，**每个文件夹下必须有一个`index.md`文件用于导航占位**，或者，**在程序中设置对应的路由选项**，比如为其设定一个默认的页面展示。

我认为，两者可以结合使用，使程序具有更强的扩展性与兼容性。

于是，**我再次新建了一些`index.md`文件**，并**再次触发了`index key 重复`的报错**。

### 摸索历程

#### 源代码检查

查看源代码，原来是`content-theme-docs`的作者，在遍历文章列表（以生成侧边栏）时，将文章的`slug`作为了键索引。

`slug`是什么鬼呢？在`gatsby`框架里`slug`就是文件的路径，即域名后面的一串`path`。

但在`nuxt`中我检查出以下的结果。

首先是`marked`包中有`slug`的生成方式，如下。

![image-20200818165256663](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200818165256663.png)



有点复杂，主要是我并不知道这里面的`e`是什么，是文件名？还是文件路径？不得而知。

接着在`content-theme-docs`中有一个`slug`的赋值操作。

```js[nuxt-content-theme-mark/index.js]
export default (userConfig) => {
  const config = defu.arrayFn(userConfig, defaultConfig)

  config.hooks['content:file:beforeInsert'] = (document) => {
    const regexp = new RegExp(
      `^/(${config.i18n.locales.map((locale) => locale.code).join('|')})`,
      'gi'
    )
    const dir = document.dir.replace(regexp, '')
    const slug = document.slug.replace(/^index/, '')

    document.to = `${dir}/${slug}`
  }

  return config
}

```

可以看到，这里的`slug`主要是从文档的`slug`属性中，替换掉`index`，并生成文档的路由地址，但貌似也不是我们想要的答案。



再接着，我们查到了`content-theme-docs`中关于导航栏生成的代码。

```vue{2,11}[nuxt-content-theme-mark/components/global/app/AppNav.vue]
   <li
          v-for="(docs, category, index) in categories"
          :key="category"
          class="mb-4"
          :class="{ 'lg:mb-0': index === Object.keys(categories).length - 1 }"
        >
          <h3
            class="mb-2 text-gray-500 uppercase tracking-wider font-bold text-sm lg:text-xs"
          >{{ category }}</h3>
          <ul>
            <li v-for="doc of docs" :key="doc.slug" class="text-gray-700 dark:text-gray-300">
```



在这里，首先对所有的目录进行遍历，这些目录其实不是文件系统中的目录，而是定义在每篇文章里`frontmatter`中的字段`category`。

```vue
    categories() {
      return this.$store.state.categories[this.$i18n.locale]
    }
```



<tip>

说到这儿，如果对`content-theme-docs`生成侧边栏的方式不满意，以及对于`frontmatter`中需要人为加入`directory`而非`tags`等设定不满意的话，可以重写一下`AppNav.vue`文件。（我自己目前还挺满意的，我并不想hack这个文件，以改变所有文章的筛选显示。我会采取`\$content()`的方法进行筛选。

</tip>



由于目录一般来说都是些不一样的文件名，而且`categories`已经是一个集合（应该是），不存在键重复的问题。

但最后一行，`doc.slug`就有问题了，因为这里的slug，很有可能就是文档名，即对于任意文件夹下的`index.md`文件，其`slug`都等于`index`，由此引发了键冲突。



### 解决方案

由于`vue`中的遍历，键只是一种索引，比较随意，因此简单修改以上源代码，将键设置为其他字段就好了，比如本地文件系统的路径地址，这个肯定唯一。

实践结果显示，浏览器确实不再报错了，解决方案有效！

### 反思总结
尽管问题解决了，但是还有一些困惑存在。
#### 1. `content-theme-docs`这个slug到底是怎么生成的，为什么只生成了文件名（而不是像其他框架一样生成了文件的相对路径）？

留待后续解决吧！

## 【已解决】BUG: element-ui默认drawer不能滚动

### 需求

我需要实现一个点击`菜单`键弹出一个`抽屉`，显示可供选择的菜单项。

### 现场

我使用了`el-drawer`帮助我完成这样的需求。

在开发过程中，我尝试放入了一个较长的列表项，这直接导致抽屉中的内容高度超过100%，并无法滚动。

问题是无法滚动，这不符合常识。

我搜索到这样的一篇帖子：[[Bug Report] el-Drawer Rolling Problem Repair · Issue #17600 · ElemeFE/element](https://github.com/ElemeFE/element/issues/17600)，原来是`drawer`内部的css设定了`overflow:hidden`。

了解到这样的信息后，我立即验证了一下，果然是这个问题。

![image-20200818160242494](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200818160242494.png)



那么解决方案就很简单了，通过各种方式修改成`overflow:auto`即可。

### 解决方案

帖子中给出的方法是修改`element-ui`的`index.css`文件，我尽量避免这样的措施。

我选择的方法是对该组件进行`css穿透`设置，详情如下。

```css
/deep/ .el-drawer {
  overflow: auto !important;
}
```



可以看到，浏览器里已经成功覆写了内部`overflow:hidden`的设定，转而变成`overflow:auto`，满足了我们滚动的需求。



### 反思总结

有几点值得注意。

#### 1. 为什么element-ui要屏蔽溢出

不得而知。

也许是他们认为打开的抽屉，不应有超出的内容，毕竟真实世界中抽屉是不可以装超过自身体积的东西的。

所以，也许有其设定的合理性。

在此共识上，我自己也尽量遵循这样的一种设定，我目前是开发阶段，所以直接装入了一个很长的列表，后续会把它们组合，尽量使之不超过整个容器体积。

但尽管如此，我还是倾向于可以竖向滚动，因此我还是在组件中使用了穿透的方式，以防万一。



#### 2. 两种穿透方式

```css
/* 方式一，使用/deep/，通用 */
/deep/ .el-drawer {
  overflow: auto !important;
}

/* 方式二，使用 >>>，sass不支持 */
>>> .el-drawer {
  overflow: auto !important;
}
```



#### 3. 穿透设置要点

以下写法是不正确的，这意味着在`el-drawer`下的一些子级元素需要进行`overflow:auto`的设定。

```css
.el-drawer /deep/ {
  overflow: auto !important;
}
```



在这里的实际需求是`el-drawer`其本身需要进行`overflow:auto`的设定。

因此，顺序很重要。



## 【已解决】BUG: prettier对jsx格式style分行后浏览器解析无法识别

## 现场

初始代码，一行jsx，定义了多个css属性。

![image-20200818132729397](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200818132729397.png)



经过`prettier`美化后，变成了多行。

![image-20200818133051185](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200818133051185.png)



然而，在浏览器里渲染出来，却多了换行符导致`css`无效。

​	![image-20200818133210747](https://mark-vue-oss.oss-cn-hangzhou.aliyuncs.com/typora/typora-user-images/image-20200818133210747.png)



## 解决方案

使用正则替换一下。

```js
const styleReplace = (s) => {
  const s2 = s
    .trim()
    .replace(/\s*(\S*): (\S*);\s*/g, "'$1': '$2',")
    .replace(/style="(.*?)"/m, ':style = "{$1}"')
  console.log(s2)
  return s2
}

```



## 反思总结

待更。
